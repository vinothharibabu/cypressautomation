// <reference types="Cypress" />
import pageObjectModel from "../support/pageObjectModel"
const objPOM = new pageObjectModel()

describe("Login to the application",function()
{
    // it("Login should be successful",function()
    // {
    //     cy.visit("https://www.guru99.com/")
    //     cy.get('[class="featured-box cloumnsize1"]>h4>b').contains('Testing')
    //     cy.get('[title="Software Testing"]').click()
    //     cy.get('[class="entry-title"]').then(function(getTitle){
    //         expect('Software Testing Tutorial: Free QA Course').to.be(getTitle.text())
    //     })
    beforeEach(()=>{
        cy.fixture('testData').then(function(getTestData){
            this.getTestData=getTestData
        })
        cy.login()
    })

    it("Parabank Application Testing",function(){
        objPOM.accountOverview(this.getTestData.accountOverviewTitle)
        // cy.get('.title').contains('Accounts Overview')
        // cy.get('[class="aboutus"]').should('be.visible')
        // cy.get('.home').should('be.visible')
        // cy.get('.contact').should('be.visible')
        // cy.get('#accountTable').should('be.visible')
    })

    it('Open New Account Testing',function(){
        objPOM.openNewAccountTesting(this.getTestData.openNewAccountTitle,this.getTestData.accountType,this.getTestData.successMessage)
        cy.logout()

        // cy.get('[href="/parabank/openaccount.htm"]').click()
        // cy.get('[class="title"]').contains('Open New Account')
        // cy.get('[value="1"]').contains('SAVINGS')
        // cy.get('[ng-model="types.selectedOption"]').select('SAVINGS')
        // cy.get('[type="submit"]').click()
        // // cy.get('[class="title"]').contains('Account Opened!')
        // cy.get('[ng-if="showResult"]>p').contains('Congratulations, your account is now open.')
        // // cy.get('[id="newAccountId"]').then(function(getID){
        // //     expect
        // // })
        // cy.get('[href="/parabank/logout.htm"]').click()
        // cy.get('[id="leftPanel"]>h2').contains('Customer Login')
    })
})
    

