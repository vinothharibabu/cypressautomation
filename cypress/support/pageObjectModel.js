// <reference types="Cypress" />
class pageObjectModel
{
    getAccountOverviewTitle()
    {
        return cy.get('.title')
    }
    getAboutUs()
    {
        return cy.get('[class="aboutus"]')
    }
    getHome()
    {
        return cy.get('.home')
    }
    getContact()
    {
        return cy.get('.contact')
    }
    getAccountTable()
    {
        return cy.get('#accountTable')
    }
    getOpenAccount()
    {
        return cy.get('[href="/parabank/openaccount.htm"]')
    }
    getSavings()
    {
        return cy.get('[value="1"]')
    }
    getSelectOption()
    {
        return cy.get('[ng-model="types.selectedOption"]')
    }
    getSubmit()
    {
        return cy.get('[type="submit"]')
    }
    getShowResult()
    {
        return cy.get('[ng-if="showResult"]>p')
    }
    getLogout()
    {
        return cy.get('[href="/parabank/logout.htm"]')
    }
    getCustomerLogin()
    {
        return cy.get('[id="leftPanel"]>h2')
    }
    openNewAccountTesting(titleName,accountType,successMessage)
    {
        this.getOpenAccount().click()
        this.getAccountOverviewTitle().contains(titleName)
        this.getSavings().contains(accountType)
        this.getSelectOption().select(accountType)
        this.getSubmit().click()
        this.getShowResult().contains(successMessage)
    }
    accountOverview(titleName)
    {
        this.getAccountOverviewTitle().contains(titleName)
        this.getAboutUs().should('be.visible')
        this.getHome().should('be.visible')
        this.getContact().should('be.visible')
        this.getAccountTable().should('be.visible')
    }
   
} export default pageObjectModel